from django.test import TestCase, Client
from django.urls import resolve
from .views import index, user_name, calculate_age
from datetime import date

# Create your tests here.
class Lab_6_Challenge_Test(TestCase):
	"""docstring for Lab_6_Challenge_Test"""
	def test_lab_6_challenge_urls_is_exist(self):
		response = Client().get('/lab-6c/')
		self.assertEqual(response.status_code, 200)

	def test_lab_6_challenge_using_template(self):
		response = Client().get('/lab-6c/')
		self.assertTemplateUsed(response, 'my_profile.html')

	def test_lab_6_challenge_user_name_exist(self):
		response = Client().get('/lab-6c/')
		html_response = response.content.decode('utf8')
		self.assertIn('<h1>Hello, my name is ' + user_name + '</h1>', html_response)
		self.assertFalse(len(user_name) == 0)

	def test_lab_6_challenge_using_index_func(self):
		found = resolve('/lab-6c/')
		self.assertEqual(found.func, index)

	def test_age_is_correct(self):
		self.assertEqual(0, calculate_age(date.today().year))
		self.assertEqual(18, calculate_age(2000))
		self.assertEqual(28, calculate_age(1990))
