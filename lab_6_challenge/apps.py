from django.apps import AppConfig


class Lab6ChallengeConfig(AppConfig):
    name = 'lab_6_challenge'
