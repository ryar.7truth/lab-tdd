from django.shortcuts import render
from datetime import datetime, date


# Enter your name here
user_name = 'Ryan Aulia Rachman'
curr_year = int(datetime.now().strftime("%Y"))
birth_date = date(1995, 9, 22)
# Create your views here.
def index(request):
    response = {'name': user_name, 'age': calculate_age(birth_date.year)}
    return render(request, 'my_profile.html', response)

def calculate_age(birth_year):
    return curr_year - birth_year if birth_year <= curr_year else 0
