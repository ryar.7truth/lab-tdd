from django.shortcuts import render, redirect
from django.http import HttpResponse, HttpResponseRedirect, JsonResponse, HttpResponseBadRequest
from .models import Subscriber as Subs
from django.views.decorators.csrf import csrf_exempt
import os
import json

# Create your views here.

sudah_ada = 'Email sudah terdaftar'
berhasil = 'Account berhasil subscribe!'

def index(request):
	return render(request, 'lab_10.html')

@csrf_exempt
def add_subs(request):
    if request.method == 'POST':
    	email = request.POST['email']
    	name = request.POST['name']
    	password = request.POST['pass']
    	if Subs.objects.filter(email = email).exists():
    		response = {'result' : sudah_ada}
    		return redirect('/lab-10/')
    		#return render(request, 'lab_10.html', response)
    	response = {'result' : berhasil}
    	subs = Subs(email=email, name=name, password=password)
    	subs.save()
    	return redirect('/lab-10/')
    	#return render(request, 'lab_10.html', response)

@csrf_exempt
def validate_email(request):
	email = request.POST.get('email', None)
	data = {
		'is_taken': Subs.objects.filter(eamil = eamil).exists()
	}
	return JsonResponse(data)
