from django.conf.urls import url
from .views import index, add_subs, validate_email

urlpatterns = [
	url(r'^$', index, name='index'),
	url(r'add-subs/$', add_subs, name='add-subs'),
	url(r'validate-email/$', validate_email, name='validate-email'),
]
