from django.contrib import admin
from .models import Subscriber as Subs

# Register your models here.
admin.site.register(Subs)
