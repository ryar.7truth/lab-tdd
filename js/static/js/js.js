var myVar;

function myFunction() {
    myVar = setTimeout(showPage, 3000);
}

function showPage() {
  document.getElementById("loader").style.display = "none";
  document.getElementById("myDiv").style.display = "block";
}

var acc = document.getElementsByClassName("accordion");
var i;

for (i = 0; i < acc.length; i++) {
  acc[i].addEventListener("click", function() {
    this.classList.toggle("active");
    var panel = this.nextElementSibling;
    if (panel.style.maxHeight){
      panel.style.maxHeight = null;
    } else {
      panel.style.maxHeight = panel.scrollHeight + "px";
    } 
  });
}

var themes = [{"id":0,"text":"Red","bcgColor":"#F44336","fontColor":"#FAFAFA"},
    {"id":1,"text":"Pink","bcgColor":"#E91E63","fontColor":"#FAFAFA"},
    {"id":2,"text":"Purple","bcgColor":"#9C27B0","fontColor":"#FAFAFA"},
    {"id":3,"text":"Indigo","bcgColor":"#3F51B5","fontColor":"#FAFAFA"},
    {"id":4,"text":"Blue","bcgColor":"#2196F3","fontColor":"#212121"},
    {"id":5,"text":"Teal","bcgColor":"#009688","fontColor":"#212121"},
    {"id":6,"text":"Lime","bcgColor":"#CDDC39","fontColor":"#212121"},
    {"id":7,"text":"Yellow","bcgColor":"#FFEB3B","fontColor":"#212121"},
    {"id":8,"text":"Amber","bcgColor":"#FFC107","fontColor":"#212121"},
    {"id":9,"text":"Orange","bcgColor":"#FF5722","fontColor":"#212121"},
    {"id":10,"text":"Brown","bcgColor":"#795548","fontColor":"#FAFAFA"}
    ];   
var selectedTheme = {"Indigo":{"bcgColor":"#3F51B5","fontColor":"#FAFAFA"}}; 

if (localStorage.getItem("selectedTheme") === null) {
    localStorage.setItem('selectedTheme', JSON.stringify(selectedTheme));
}

localStorage.setItem('themes', JSON.stringify(themes));
    
var retrievedObject = localStorage.getItem('themes');
$('.my-select').select2({data: JSON.parse(retrievedObject)});
    
var retrievedSelected = JSON.parse(localStorage.getItem('selectedTheme'));
var key;
var bcgColor;
var fontColor;
for (key in retrievedSelected) {
    if (retrievedSelected.hasOwnProperty(key)) {
        bcgColor = retrievedSelected[key].bcgColor;
        fontColor = retrievedSelected[key].fontColor;
    }
}  

$('.apply-button').on('click', function(){
    var valueTheme = $('.my-select').val();
    console.log(valueTheme);
    var theme;
    var a;
    var selectedTheme = {};
    for(a in themes){
        if(a==valueTheme){
            var bcgColor = themes[a].bcgColor;
            var fontColor = themes[a].fontColor;
            var text = themes[a].text;
            selectedTheme[text] = {"bcgColor":bcgColor, "fontColor":fontColor};
            localStorage.setItem('selectedTheme', JSON.stringify(selectedTheme));
        }
    }
});

$(document).ready(function(){
    $("button").click(function(){
        $.getJSON("demo_ajax_json.js", function(result){
            $.each(result, function(i, field){
                $("div").append(field + " ");
            });
        });
    });
});
