"""LabTDD URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.1/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.conf.urls import include
from django.views.generic import RedirectView
from django.urls import re_path
from django.contrib import admin
from lab_6.views import index as index_l6
import lab_6.urls as lab_6
import lab_6_challenge.urls as lab_6c
import js.urls as js
import lab_10.urls as lab_10
import lab_11.urls as lab_11

urlpatterns = [
    re_path(r'^admin/', admin.site.urls),
    re_path(r'^lab-6/', include(lab_6)),
    re_path(r'^lab-6c/', include(lab_6c)),
    re_path(r'^js/', include(js)),
    re_path(r'^lab-10/', include(lab_10)),
    re_path(r'^lab-11/', include(lab_11)),
    re_path(r'^$', RedirectView.as_view(url='/lab-11/'))
]
