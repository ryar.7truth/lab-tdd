from django.shortcuts import render
from django.http import HttpResponse, HttpResponseRedirect, JsonResponse
from django.views.decorators.csrf import csrf_exempt
from .models import Favourite
import os

response = {}

def index(request):
	mahasiswa_list = csui_helper.instance.get_mahasiswa_list()

    friend_list = Friend.objects.all()
    html = 'ajax.html'
    return render(request, html, response)

def friend_list(request):
    friend_list = Friend.objects.all()
    response['friend_list'] = friend_list
    html = 'lab_7/daftar_teman.html'
    return render(request, html, response)
    
def favourite_json(request): # update
    book = [obj.as_dict() for obj in Favourite.objects.all()]
    return JsonResponse({"results": book}, content_type='application/json')

@csrf_exempt
def add_favourite(request):
    if request.method == 'POST':
        npm = request.POST['npm']
        book = Friend(friend_name=name, npm=npm)
        friend.save()
        return JsonResponse(friend.as_dict())


def delete_favourite(request, book_id):
    Favourite.objects.filter(id=book_id).delete()
    return HttpResponseRedirect('/ajax/')
