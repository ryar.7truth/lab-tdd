from django.conf.urls import url
from .views import index, add_favourite


urlpatterns = [
	url(r'^$', index, name='index'),
	url(r'^add-favourite/$', add_favourite, name="add-favourite"),
	url(r'^delete-favourite/(?P<book_id>[0-9]+)/$', delete_avourite, name="delete-favourite"),
	url(r'^get-favourite/$', favourite_json, name="get-favourite")
]
