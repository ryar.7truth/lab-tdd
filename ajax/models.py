from __future__ import unicode_literals
from django.db import models


# Create your models here.
class Favourite(models.Model):
    title = models.CharField(max_length=400)
    author = models.CharField(max_length=400)
    publisher = models.CharField(max_length=400)
    published = models.CharField(max_length=400)

    def as_dict(self):
        return {
            "title": self.title,
            "author": self.author,
            "publisher": self.publisher,
            "published": self.published,
        }
