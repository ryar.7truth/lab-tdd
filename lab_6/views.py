from django.shortcuts import render, redirect
from .models import MyStatus as Status
from datetime import datetime
import pytz
import json


# Create your views here.
my_status = {}
def index(request):
	my_status = Status.objects.all().values()
	return render(request, 'my_status.html', {'my_status' : convert_queryset_into_json(my_status)})

def add_activity(request):
	if request.method == 'POST':
		date = datetime.strptime(request.POST['date'],'%Y-%m-%dT%H:%M')
		Status.objects.create(date=date.replace(tzinfo=pytz.UTC), activity=request.POST['activity'], place=request.POST['place'], category=request.POST['category'])
		return redirect('/lab-6/')

def delete_activity(request):
	if request.method == 'POST':
		Status.objects.all().delete()
		return redirect('/lab-6/')

def convert_queryset_into_json(queryset):
	ret_val = []
	for data in queryset:
		ret_val.append(data)
	return ret_val
