from django.conf.urls import url
from .views import index, add_activity, delete_activity


urlpatterns = [
	url(r'^$', index, name='index'),
	url(r'add_activity/$', add_activity, name='add_activity'),
	url(r'delete_activity/$', delete_activity, name='delete_activity'),
]
