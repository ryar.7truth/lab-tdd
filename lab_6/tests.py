from django.test import TestCase, Client, LiveServerTestCase
from django.urls import resolve
from django.utils import timezone
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.chrome.options import Options
from .views import index
from .models import MyStatus as Status
import time


# Create your tests here.
class Lab_6_Test(TestCase):
	"""docstring for Lab_6_Test"""
	def test_lab_6_urls_is_exist(self):
		response = Client().get('/lab-6/')
		self.assertEqual(response.status_code, 200)

	def test_lab_6_using_template(self):
		response = Client().get('/lab-6/')
		self.assertTemplateUsed(response, 'my_status.html')

	def test_lab_6_text_exist(self):
		response = Client().get('/lab-6/')
		html_response = response.content.decode('utf8')
		self.assertIn('Hello, Apa kabar?', html_response)

	def test_lab_6_using_index_func(self):
		found = resolve('/lab-6/')
		self.assertEqual(found.func, index)

	def test_model_can_create_new_activity(self):
		#Create a new activity
		new_activity = Status.objects.create(date=timezone.now(), activity='Main Dragalia Lost', place='Mebu', category='Gaming')
		#Retrieve all available activity
		counting_all_available_activity = Status.objects.all().count()
		self.assertEqual(counting_all_available_activity, 1)

	def test_can_save_a_POST_request(self):
		response = self.client.post('/lab-6/add_activity/', data={'date': '2018-10-12T11:12', 'activity': 'Maen BanGDream!', 'place': 'Pacil', 'category': 'Giming'})
		counting_all_available_activity = Status.objects.all().count()
		self.assertEqual(counting_all_available_activity, 1)
		self.assertEqual(response.status_code, 302)
		self.assertEqual(response['location'], '/lab-6/')

		new_response = self.client.get('/lab-6/')
		html_response = new_response.content.decode('utf8')
		self.assertIn('Maen BanGDream!', html_response)

	def test_can_delete_a_POST_request(self):
		response = self.client.post('/lab-6/add_activity/', data={'date': '2018-10-12T11:12', 'activity': 'Maen BanGDream!', 'place': 'Pacil', 'category': 'Giming'})
		counting_all_available_activity = Status.objects.all().count()
		self.assertEqual(counting_all_available_activity, 1)
		self.assertEqual(response.status_code, 302)
		self.assertEqual(response['location'], '/lab-6/')

		new_response = self.client.get('/lab-6/')
		html_response = new_response.content.decode('utf8')
		self.assertIn('Maen BanGDream!', html_response)

		final_response = self.client.post('/lab-6/delete_activity/')
		counting_all_available_activity = Status.objects.all().count()
		self.assertEqual(counting_all_available_activity, 0)
		self.assertEqual(response.status_code, 302)
		self.assertEqual(response['location'], '/lab-6/')


# class Lab_6_Functional_Test(LiveServerTestCase):
# 	def setUp(self):
# 		chrome_options = Options()
# #		chrome_options.add_argument('--dns-prefetch-disable')
# #		chrome_options.add_argument('--no-sandbox')
# #		chrome_options.add_argument('--headless')
# #		chrome_options.add_argument('disable-gpu')
# 		self.selenium = webdriver.Chrome('./chromedriver.exe' ,chrome_options=chrome_options)
# 		super(Lab_6_Functional_Test, self).setUp()

# 	def tearDown(self):
# 		time.sleep(3)
# 		self.selenium.quit()
# 		super(Lab_6_Functional_Test, self).tearDown()

# 	def test_input_status(self):
# 		selenium = self.selenium
# 		# Opening the link we want to test
# 		# selenium.get(self.live_server_url)
# 		selenium.get('http://127.0.0.1:8000/lab-6/')
# 		# find the form element
# 		date = selenium.find_element_by_id('date')
# 		activity = selenium.find_element_by_id('activity')
# 		place = selenium.find_element_by_id('place')
# 		category = selenium.find_element_by_id('category')
# 		submit = selenium.find_element_by_id('submit')

# 		date.send_keys('06-06-2018')
# 		date.send_keys(Keys.TAB)
# 		date.send_keys('07:06')
# 		activity.send_keys('Coba Coba')
# 		place.send_keys('Coba Coba')
# 		category.send_keys('Coba Coba')
# 		submit.send_keys(Keys.RETURN)

# 		status = selenium.find_element_by_class_name("activity").text
# 		self.assertIn("Coba Coba", status)

# 	def test_title_exist(self):
# 		selenium = self.selenium
# 		selenium.get('http://127.0.0.1:8000/lab-6/')

# 		title = selenium.find_element_by_id("title").text
# 		self.assertIn("Hello, Apa kabar?", title)
